import mymodule

import torch
from tqdm import trange
import random 

from torch.utils.tensorboard import SummaryWriter
import torchvision.transforms.functional as TF
import torch.nn.functional as F
import numpy as np 
import cv2
import os 

# モデルのパラメータを保存
param_path = input("param path>>>")
net = mymodule.Net(10)
net.load_state_dict(torch.load(param_path))

# 【TensorBoard 】結果の定量的評価
net.to("cuda:0")
res_list = []
my_text =""
for k in range(10):
    cnt = 0
    for i in trange(1000):
        img = mymodule.make_line_image(k)
        img = TF.to_tensor(img).to("cuda:0")
        img = torch.unsqueeze(img, 0)
        res = net(img)
        dl = F.softmax(res, dim=1).tolist()[0]
        detect = np.argmax(dl)
        if k == detect:
            cnt += 1
    my_text += f"ラベル{k}正解率：{cnt / 1000 * 100:.2f} %  \r\n"
    res_list.append(cnt / 1000 * 100)
net.to("cpu")
my_text += f"全体正解率：{sum(res_list) / 10:.2f} %"
print(my_text)