import cv2 
import numpy as np
import matplotlib.pyplot as plt
import random
import torch

from torchvision import transforms, datasets
import torchvision.transforms.functional as TF
import torchvision

import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


def make_line_image(n:int):
    """
    与えられた本数(n)の線が書かれた画像を返す
    """
    size = 128
    img = np.zeros((size, size, 3), dtype="uint8")
    for i in range(n):
        x_1 = random.randrange(0, size)
        y_1 = random.randrange(0, size)
        x_2 = random.randrange(0, size)
        y_2 = random.randrange(0, size)
        cv2.line(img, (x_1, y_1), (x_2, y_2), (255,255,255), thickness=1)
    return img

class myDataset(torch.utils.data.Dataset):
    """
    線が0 ~ 5本描かれた画像と線の本数をセットで返すようなデータセット
    """
    def __init__(self, ids=100):
        self.ids = ids

    def __getitem__(self, index):
        my_annotation = {}
        n = random.randrange(10)
        img = make_line_image(n)
        my_annotation["labels"] = n
        # 画像をTensorにして渡す
        img = TF.to_tensor(img)
            
        return img, my_annotation

    def __len__(self):
        return self.ids

def choice_model(model_name):
    """
    入力された名前のモデルを返す
    """
    model_dict = {
        "default": default_Net(),
        "dropout": dropout_Net(),
        "deep" : deep_Net(),
        "vgg_16": vgg_16_Netf()
    }
    return model_dict[model_name]


class default_Net(nn.Module):
    def __init__(self):
        super(default_Net, self).__init__()
        # ここの3が色のRGBに対応している部分
        self.conv1 = nn.Conv2d(3, 10, 3) # 128x128 -> 126x126
        # padding=1にすればよい？
        self.pool = nn.MaxPool2d(2, 2) # 
        self.conv2 = nn.Conv2d(10, 10, 3)
        self.fc1 = nn.Linear(10 * 30 * 30, 120)
        self.fc2 = nn.Linear(120, 84)
        # ここの5が識別対象のラベルが5種類であることに対応している
        self.fc3 = nn.Linear(84, 10) 
        self.dropout1 = torch.nn.Dropout2d(p=0.3)

        
    def forward(self, x):
        x = F.relu(self.conv1(x)) # 3x128x128 -> 10x126x126
        x = self.pool(F.relu(self.conv2(x))) # 10x126x126 -> 10x124x124 -> 10x62x62
        x = self.pool(F.relu(self.conv2(x))) # 10x62x62 -> 10x60x60 -> 10x30x30
        x = x.view(-1, 10 * 30 * 30) # 10x30x30 -> 9000
        x = F.relu(self.fc1(x)) # 9000 -> 120
        x = F.relu(self.fc2(x)) # 120 -> 84
        x = self.fc3(x) # 84 -> out_num
        return x


class dropout_Net(nn.Module):
    def __init__(self):
        super(dropout_Net, self).__init__()
        # ここの3が色のRGBに対応している部分
        self.conv1 = nn.Conv2d(3, 10, 3) # 128x128 -> 126x126
        # padding=1にすればよい？
        self.pool = nn.MaxPool2d(2, 2) # 
        self.conv2 = nn.Conv2d(10, 10, 3)
        self.fc1 = nn.Linear(10 * 30 * 30, 120)
        self.fc2 = nn.Linear(120, 84)
        # ここの5が識別対象のラベルが5種類であることに対応している
        self.fc3 = nn.Linear(84, 10) 
        self.dropout1 = torch.nn.Dropout2d(p=0.3)
        self.dropout2 = torch.nn.Dropout2d(p=0.3)

        
    def forward(self, x):
        x = F.relu(self.conv1(x)) # 3x128x128 -> 10x126x126
        x = self.pool(F.relu(self.conv2(x))) # 10x126x126 -> 10x124x124 -> 10x62x62
        x = self.dropout1(x)
        x = self.pool(F.relu(self.conv2(x))) # 10x62x62 -> 10x60x60 -> 10x30x30
        x = self.dropout2(x)
        x = x.view(-1, 10 * 30 * 30) # 10x30x30 -> 9000
        x = F.relu(self.fc1(x)) # 9000 -> 120
        x = F.relu(self.fc2(x)) # 120 -> 84
        x = self.fc3(x) # 84 -> out_num
        return x


class deep_Net(nn.Module):
    def __init__(self):
        super(deep_Net, self).__init__()
        # ここの3が色のRGBに対応している部分
        self.conv1 = nn.Conv2d(3, 20, 3, padding=1) 
        self.conv2 = nn.Conv2d(20, 20, 3, padding=1) 
        self.conv3 = nn.Conv2d(20, 20, 3, padding=1) 
        self.pool = nn.MaxPool2d(2, 2) # 128 -> 64
        self.conv4 = nn.Conv2d(20, 20, 3, padding=1) 
        self.conv5 = nn.Conv2d(20, 10, 3, padding=1) 
        self.fc1 = nn.Linear(10 * 32 * 32, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10) 
        self.dropout1 = torch.nn.Dropout2d(p=0.3)

        
    def forward(self, x):
        x = F.relu(self.conv1(x)) 
        x = F.relu(self.conv2(x)) 
        x = self.dropout1(x)
        x = F.relu(self.conv3(x)) 
        x = self.pool(x) 
        x = F.relu(self.conv4(x)) 
        x = self.dropout1(x)
        x = F.relu(self.conv5(x)) 
        x = self.pool(x) 
        x = x.view(-1, 10 * 32 * 32) # 10x30x30 -> 9000
        x = F.relu(self.fc1(x)) # 9000 -> 120
        x = F.relu(self.fc2(x)) # 120 -> 84
        x = self.fc3(x) # 84 -> out_num
        return x

def vgg_16_Netf():
    net_ft1 = torchvision.models.vgg16(pretrained=True)
    num_ftrs = net_ft1.classifier[6].in_features
    net_ft1.classifier[6] = torch.nn.Linear(num_ftrs, 10)
    return net_ft1

