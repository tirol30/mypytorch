import mymodule

import torch
from tqdm import trange
import random 

from torch.utils.tensorboard import SummaryWriter
import torchvision.transforms.functional as TF
import torch.nn.functional as F
import numpy as np 
import cv2
import os 

# モデルの名前
# model_name = "default"
model_name = "vgg_16"

# 変数の準備
root_dir = os.path.dirname(os.path.abspath(__file__))

# 【TensorBoard】タイムスタンプ用
import datetime
dt_now = datetime.datetime.now()
date_str = dt_now.strftime('%Y-%m-%dT%H-%M-%SZ')

# 重要なパラメータ
epochs = 50
dataset_num = 3000
batch_size = 10 
learning_rate = 0.005

# 【表示用、TensorBoard】記録用変数
print_num = 200
write_num = 20

# 学習用データセットの準備
train_set = mymodule.myDataset(dataset_num) # データセットができる
train_loader = torch.utils.data.DataLoader(train_set,batch_size=10) # データローダーを作る

# 【TensorBoard】tensorboardの準備
log_dir = os.path.join(root_dir, "logs",model_name,  f"{date_str}")
writer = SummaryWriter(log_dir=log_dir)

try:
    # 学習の準備
    net = mymodule.choice_model(model_name) # モデルを用意
    net.to("cuda:0") # モデルをGPUに移す
    optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate, momentum=0.9) # 最適化は確率的勾配降下法（SGD）で行う
    criterion = torch.nn.CrossEntropyLoss() # 識別モデルなのでクロスエントロピー誤差を求める

    # 【TensorBoard】モデルのグラフを作成してtensorboardに表示
    image = torch.randn(1,3,128,128).to("cuda:0")
    writer.add_graph(net, image)

    # 【TensorBoard】記録用変数
    cnt = 0

    # 学習を実行
    for epoch in range(epochs):
        # 【表示用、TensorBoard】記録用変数
        running_loss = 0.0
        running_loss2 = 0.0

        for i, data in enumerate(train_loader):
            # 学習メイン部分
            inputs, ann = data
            optimizer.zero_grad()
            inputs = inputs.to("cuda:0")
            outputs = net(inputs).to("cpu")
            loss = criterion(outputs, ann["labels"])
            loss.backward()
            optimizer.step()
            
            # 【表示用】lossをコマンドラインに表示   
            running_loss += loss.item()
            if i % print_num == print_num -1 :
                print('[%d, %5d] loss: %.7f' %
                    (epoch + 1, i + 1, running_loss / (print_num * batch_size)))
                running_loss = 0.0

            # 【TensorBoard】lossを書き込み 
            running_loss2 += loss.item()
            cnt += batch_size // 10
            if i % write_num == write_num -1: 
                writer.add_scalar(f"loss", running_loss2 / (write_num * batch_size) , cnt)
                running_loss2 = 0.0

    # 【表示用】学習終了を通知し、モデルをＣＰＵへ移動
    print('Finished Training')
    net.to("cpu")

    # モデルのパラメータを保存
    save_dir = os.path.join(root_dir, "param", model_name)
    os.makedirs(save_dir, exist_ok = True)
    param_path = os.path.join(root_dir, "param", model_name,  f"{date_str}.pth")
    torch.save(net.state_dict(), param_path)

    # 【TensorBoard】画像をTensorBoardに表示
    for i in range(10):
        img = mymodule.make_line_image(i)
        img_tf = TF.to_tensor(img)
        img_tf = torch.unsqueeze(img_tf, 0)
        res = net(img_tf)
        dl = F.softmax(res, dim=1).tolist()[0]
        detect = np.argmax(dl)
        cv2.putText(img, f"pred={detect}", (0,15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 0), thickness=1)
        writer.add_image(f'images_{i}', TF.to_tensor(img), 0)


    # 【TensorBoard 】結果の定量的評価
    net.to("cuda:0")
    res_list = []
    my_text =""
    for k in range(10):
        cnt = 0
        for i in trange(1000):
            img = mymodule.make_line_image(k)
            img = TF.to_tensor(img).to("cuda:0")
            img = torch.unsqueeze(img, 0)
            res = net(img)
            dl = F.softmax(res, dim=1).tolist()[0]
            detect = np.argmax(dl)
            if k == detect:
                cnt += 1
        my_text += f"ラベル{k}正解率：{cnt / 1000 * 100:.2f} %  \r\n"
        res_list.append(cnt / 1000 * 100)
    net.to("cpu")
    my_text += f"全体正解率：{sum(res_list) / 10:.2f} %"
    writer.add_text("評価結果", my_text)

except Exception as e:
    print(e)
finally:
    # 【TensorBoard】writerを閉じる
    writer.close() 
